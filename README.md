
# the-tvdb-api

> A simple library to access The TVDB data

## Requirements

* [PHP](https://secure.php.net/manual/en/install.php) >= 5.3.29
* [php-curl-class](https://github.com/php-curl-class/php-curl-class) >= 8.0

## Install

### Composer
```bash
composer require nihilarr/the-tvdb-api
```

## Usage
```php
require __DIR__ . '/vendor/autoload.php';

use Nihilarr\TheTvdbApi;

$tvdb = new TheTvdbApi('api_key', 'user_key', 'username');

if($tvdb->authenticate()) {
  // Do lookups
}

```

```php
  $series = $tvdb->series("153021");
  var_dump($series);
}
```

```php
  $episode = $tvdb->episode("2656091");
  var_dump($episode);
}
```

```php
  $episode = $tvdb->episode_by_season_episode("the walking dead", 1, 4);
  var_dump($episode);
}
```

### Note

## Contributing

## License

MIT © [Drew Smith](https://www.nihilarr.com)
